using Urs.Core;
namespace Urs.Data.Domain.Directory
{
    /// <summary>
    /// Represents a state/province
    /// </summary>
    public partial class Area : BaseEntity
    {
        public string ProvinceCode { get; set; }
        public string ProvinceName { get; set; }
        public string CityCode { get; set; }
        public string CityName { get; set; }
        public string AreaCode { get; set; }
        public string AreaName { get; set; }
        public bool Published { get; set; }
        public int DisplayOrder { get; set; }
    }

}
