using Urs.Core;
using System;
using Urs.Data.Domain.Stores;
using Urs.Data.Domain.Users;

namespace Urs.Data.Domain.Orders
{
    /// <summary>
    /// 购物车
    /// </summary>
    public partial class ShoppingCartItem : BaseEntity
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public virtual int UserId { get; set; }
        /// <summary>
        /// 产品Id
        /// </summary>
        public virtual int GoodsId { get; set; }
        /// <summary>
        /// 产品属性Xml
        /// </summary>
        public virtual string AttributesXml { get; set; }
        /// <summary>
        /// 选择
        /// </summary>
        public virtual bool Selected { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public virtual int Quantity { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public virtual DateTime CreateTime { get; set; }
        public virtual DateTime UpdateTime { get; set; }
        public virtual Goods Goods { get; set; }

        /// <summary>
        /// 用户
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// 免配送
        /// </summary>
        public virtual bool IsFreeShipping
        {
            get
            {
                var goods = this.Goods;
                if (goods != null)
                    return goods.IsFreeShipping;
                return true;
            }
        }

        /// <summary>
        /// 是否配送
        /// </summary>
        public virtual bool IsShipEnabled
        {
            get
            {
                var goods = this.Goods;
                if (goods != null)
                    return goods.IsShipEnabled;
                return false;
            }
        }
        /// <summary>
        ///额外配送费
        /// </summary> 
        public virtual decimal AdditionalShippingCharge
        {
            get
            {
                decimal shippingFee = decimal.Zero;
                var goods = this.Goods;
                if (goods != null)
                    shippingFee = goods.AdditionalShippingCharge * Quantity;
                return shippingFee;
            }
        }

    }
}
