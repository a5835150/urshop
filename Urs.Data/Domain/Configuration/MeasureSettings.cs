﻿
using Urs.Core.Configuration;

namespace Urs.Data.Domain.Configuration
{
    public class MeasureSettings : ISettings
    {
        public int BaseDimensionId { get; set; }
        public int BaseWeightId { get; set; }
    }
}