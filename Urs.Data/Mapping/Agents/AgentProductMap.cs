using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Agents;

namespace Urs.Data.Mapping.Agents
{
    public partial class AgentProductMap : UrsEntityTypeConfiguration<AgentGoods>
    {
        public override void Configure(EntityTypeBuilder<AgentGoods> builder)
        {
            builder.ToTable("Agentproduct");
            builder.HasKey(p => p.Id);

            base.Configure(builder);
        }
    }
}
