
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Logging;

namespace Urs.Data.Mapping.Logging
{
    public partial class LogMap : UrsEntityTypeConfiguration<Log>
    {
        public override void Configure(EntityTypeBuilder<Log> builder)
        {
            builder.ToTable(nameof(Log));
            builder.HasKey(l => l.Id);
            builder.Property(l => l.ShortMessage).IsRequired();
            builder.Property(l => l.FullMessage);
            builder.Property(l => l.IpAddress).HasMaxLength(200);

            builder.Ignore(l => l.LogLevel);

            builder.HasOne(l => l.User)
                .WithMany()
                .HasForeignKey(l => l.UserId)
            .OnDelete(DeleteBehavior.Cascade);
            base.Configure(builder);

        }
    }
}