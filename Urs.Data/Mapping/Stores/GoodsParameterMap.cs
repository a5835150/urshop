
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Mapping.Stores
{
    public partial class GoodsParameterMap : UrsEntityTypeConfiguration<GoodsParameterMapping>
    {
        public override void Configure(EntityTypeBuilder<GoodsParameterMapping> builder)
        {
            builder.ToTable(nameof(GoodsParameterMapping));
            builder.HasKey(psa => psa.Id);

            builder.Property(psa => psa.CustomValue).HasMaxLength(4000);

            builder.HasOne(psa => psa.GoodsParameterOption)
                .WithMany(sao => sao.GoodsParameters)
                .HasForeignKey(psa => psa.GoodsParameterOptionId);
            
            base.Configure(builder);
        }
    }
}