
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Mapping.Stores
{
    public partial class GoodsBrandMappingMap : UrsEntityTypeConfiguration<GoodsBrandMapping>
    {
        public override void Configure(EntityTypeBuilder<GoodsBrandMapping> builder)
        {
            builder.ToTable(nameof(GoodsBrandMappingMap));
            builder.HasKey(pm => pm.Id);
            
            builder.HasOne(pm => pm.Goods)
                .WithMany()
                .HasForeignKey(pm => pm.GoodsId);
            base.Configure(builder);
        }
    }
}