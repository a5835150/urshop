﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;

namespace Urs.Plugin.Weixin.MiniSDK.SwaggerGen
{
    /// <summary>
    /// Swagger注释帮助类
    /// </summary>
    public partial class SwaggerDocTag : IDocumentFilter
    {
        /// <summary>
        /// 添加附加注释
        /// </summary>
        /// <param name="swaggerDoc"></param>
        /// <param name="context"></param>
        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            if (swaggerDoc.Tags == null) swaggerDoc.Tags = new List<Tag>();
            swaggerDoc.Tags.Add(new Tag() { Name = "WeixinMini", Description = "微信小程序SDK" });
        }
    }
}
