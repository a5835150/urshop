﻿using Urs.Framework;
using Urs.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urs.Framework.Models;

namespace Urs.Plugin.Shipping.ByWeight.Models
{
    public class ShippingScopeByWeightModel : BaseEntityModel
    {
        [UrsDisplayName("Admin.Address.Fields.ProvinceId")]
        public int ProvinceId { get; set; }
        [UrsDisplayName("Admin.Address.Fields.ProvinceName")]
        public string ProvinceName { get; set; }

        [UrsDisplayName("Admin.Address.Fields.CityId")]
        public int? CityId { get; set; }
        [UrsDisplayName("Admin.Address.Fields.CityName")]
        public string CityName { get; set; }

        public int ShippingByWeightRecordId { get; set; }

        [UrsDisplayName("Plugins.Shipping.ByWeight.Fields.DivisionName")]
        public string DivisionName { get; set; }
    }
}
