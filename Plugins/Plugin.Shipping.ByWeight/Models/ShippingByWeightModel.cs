﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Urs.Framework;
using Urs.Framework.Models;

namespace Urs.Plugin.Shipping.ByWeight.Models
{
    public class ShippingByWeightModel : BaseEntityModel
    {
        public ShippingByWeightModel()
        {
            Scopes = new List<EditShippingScopeModel>();
            AvailableShippingMethods = new List<SelectListItem>();
            City = new List<string>();
        }
        [UrsDisplayName("Plugins.Shipping.ByWeight.Fields.AreasName")]
        public string AreasName { get; set; }
        
        [UrsDisplayName("Plugins.Shipping.ByWeight.Fields.ShippingMethod")]
        public int ShippingMethodId { get; set; }

        [UrsDisplayName("Plugins.Shipping.ByWeight.Fields.ShippingMethod")]
        [UIHint("SelectShippingMethod")]
        public string ShippingMethodName { get; set; }

        [UrsDisplayName("Plugins.Shipping.ByWeight.Fields.First")]
        public decimal First { get; set; }

        [UrsDisplayName("Plugins.Shipping.ByWeight.Fields.FirstPrice")]
        public decimal FirstPrice { get; set; }

        [UrsDisplayName("Plugins.Shipping.ByWeight.Fields.Plus")]
        public decimal Plus { get; set; }

        [UrsDisplayName("Plugins.Shipping.ByWeight.Fields.PlusPrice")]
        public decimal PlusPrice { get; set; }

        public string BaseWeightIn { get; set; }

        public List<EditShippingScopeModel> Scopes { get; set; }
        public List<string> City { get; set; }
        public IList<SelectListItem> AvailableShippingMethods { get; set; }

    }
}