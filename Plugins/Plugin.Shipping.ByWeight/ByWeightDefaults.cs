﻿
namespace Urs.Plugin.Shipping.ByWeight
{
    /// <summary>
    /// Represents constants of the "Fixed or by weight" shipping plugin
    /// </summary>
    public static class ByWeightDefaults
    {
        /// <summary>
        /// The key of the settings to save fixed rate of the shipping method
        /// </summary>
        public const string FixedRateSettingsKey = "ShippingRateComputationMethod.ByWeight.Rate.ShippingMethodId{0}";
    }
}
