using Urs.Core.Configuration;

namespace Urs.Plugin.Payments.WeixinOpen
{
    /// <summary>
    /// Represents a Weixin wap payment settings
    /// </summary>
    public class WeixinOpenPaymentSettings : ISettings
    {
        /// <summary>
        /// 绑定支付的APPID（必须配置）
        /// </summary>
        public string AppId { get; set; }
        /// <summary>
        /// 公众帐号secert（仅JSAPI支付的时候需要配置）
        /// </summary>
        public string AppSecret { get; set; }
        /// <summary>
        /// 商户支付密钥，参考开户邮件设置（必须配置）
        /// </summary>
        public string AppKey { get; set; }
        /// <summary>
        /// 商户号（必须配置）
        /// </summary>
        public string Partner { get; set; }
    }
}