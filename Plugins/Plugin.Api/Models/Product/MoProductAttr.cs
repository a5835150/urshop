﻿using System.Collections.Generic;

namespace Plugin.Api.Models.Goods
{
    /// <summary>
    /// 多规格
    /// </summary>
    public class MoGoodsAttrList
    {
        public MoGoodsAttrList()
        {
            attrs = new List<MoGoodsAttr>();
            tables = new List<MoAttrTable>();
        }
        /// <summary>
        /// 多规格项
        /// </summary>
        public IList<MoGoodsAttr> attrs { get; set; }
        /// <summary>
        /// 多规格 （含价格库存）
        /// </summary>
        public IList<MoAttrTable> tables { get; set; }
    }
    /// <summary>
    /// 多规格项
    /// </summary>
    public class MoGoodsAttr
    {
        public MoGoodsAttr()
        {
            attrValArr = new List<GoodsAttrValue>();
        }
        /// <summary>
        /// 价格
        /// </summary>
        public int value { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        public IList<GoodsAttrValue> attrValArr { get; set; }

        public partial class GoodsAttrValue
        {
            public int value { get; set; }
            public string name { get; set; }
        }
    }
    /// <summary>
    /// 多规格（含价格库存）
    /// </summary>
    public class MoAttrTable
    {
        public MoAttrTable()
        {
            arrs = new List<AttrValueItem>();
            info = new AttrInfo();
        }
        public AttrInfo info { get; set; }

        public IList<AttrValueItem> arrs { get; set; }

        public partial class AttrInfo
        {
            public decimal price { get; set; }
            public int qty { get; set; }
            public string img { get; set; }
            public string sku { get; set; }
        }

        public partial class AttrValueItem
        {
            public int value { get; set; }
            public string name { get; set; }
        }
    }
}
