﻿namespace Plugin.Api.Models.User
{
    /// <summary>
    /// 用户头像
    /// </summary>
    public partial class MoUploadAvatar
    {
        public string base64Str { get; set; }
    }
}