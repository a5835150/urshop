﻿using System;
using System.Collections.Generic;
using Plugin.Api.Models.Common;

namespace Plugin.Api.Models.User
{
    /// <summary>
    /// 用户的邀请
    /// </summary>
    public partial class MoInvitees
    {
        public MoInvitees()
        {
            Items = new List<MoInvitee>();
            Paging = new MoPaging();
        }
        /// <summary>
        /// 邀请明细
        /// </summary>
        public IList<MoInvitee> Items { get; set; }
        /// <summary>
        /// 分页
        /// </summary>
        public MoPaging Paging { get; set; }

        #region Nested classes
        /// <summary>
        /// 邀请记录
        /// </summary>
        public partial class MoInvitee
        {
            /// <summary>
            /// 昵称
            /// </summary>
            public string Nickname { get; set; }
            /// <summary>
            /// 头像
            /// </summary>
            public string ImgUrl { get; set; }
            /// <summary>
            /// 手机号
            /// </summary>
            public string Phone { get; set; }
            /// <summary>
            /// 邀请日期
            /// </summary>
            public DateTime Time { get; set; }
        }

        #endregion
    }
}