using Microsoft.AspNetCore.Http;
using Urs.Core.Plugins;
using Urs.Data.Domain.Orders;

namespace Urs.Services.Payments
{
    public partial interface IPaymentMethod : IPlugin
    {
        #region Methods
        ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest);

        void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest);

        decimal GetAdditionalHandlingFee(decimal subTotal);

        CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest);

        RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest);

        VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest);

        bool CanRePostProcessPayment(Order order);

        #endregion

        #region Properties

        bool SupportCapture { get; }

        bool SupportPartiallyRefund { get; }

        bool SupportRefund { get; }
        bool SupportVoid { get; }
        PaymentMethodType PaymentMethodType { get; }

        #endregion
    }
}
