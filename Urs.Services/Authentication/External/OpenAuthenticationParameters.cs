using System;

namespace Urs.Services.Authentication.External
{
    [Serializable]
    public abstract partial class OpenAuthParameters
    {
        public abstract string ProviderSystemName { get; }
        public string OpenId { get; set; }
        public string OAuthToken { get; set; }
        public string OAuthRefreshToken { get; set; }
        public string UnionId { get; set; }
        public UserClaims UserClaim { get; set; }
    }
}