using Microsoft.AspNetCore.Routing;
using System.Collections.Generic;
using Urs.Core.Plugins;

namespace Urs.Services.Common
{
    public partial interface IMiscPlugin : IPlugin
    {
        IList<string> GetWidgetZones();

        string GetWidgetViewComponentName(string widgetZone);
    }
}