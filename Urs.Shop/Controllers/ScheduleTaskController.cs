﻿using Microsoft.AspNetCore.Mvc;
using Urs.Services.Logging;
using Urs.Services.Tasks;

namespace Urs.Web.Controllers
{
    public partial class ScheduleTaskController : Controller
    {
        private readonly IScheduleTaskService _scheduleTaskService;
        private readonly ILogger _logger;

        public ScheduleTaskController(IScheduleTaskService scheduleTaskService,
            ILogger logger)
        {
            this._scheduleTaskService = scheduleTaskService;
            this._logger = logger;
        }

        [HttpPost]
        public virtual IActionResult RunTask(string taskType)
        {
            var scheduleTask = _scheduleTaskService.GetTaskByType(taskType);
            if (scheduleTask == null)
                //schedule task cannot be loaded
                return NoContent();

            _logger.Information(scheduleTask.Name);

            var task = new Task(scheduleTask);
            task.Execute();

            return NoContent();
        }
    }
}