﻿using FluentValidation;
using Urs.Admin.Models.Common;
using Urs.Services.Localization;
using Urs.Framework.Validators;

namespace Urs.Admin.Validators.Plugins
{
    public class PluginValidator : BaseUrsValidator<PluginModel>
    {
        public PluginValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.FriendlyName).NotNull().WithMessage(localizationService.GetResource("Admin.Configuration.Plugins.Fields.FriendlyName.Required"));
        }
    }
}