﻿using Microsoft.AspNetCore.Mvc;
using Urs.Framework;
using Urs.Framework.Mvc;

using Urs.Framework.Kendoui;

namespace Urs.Admin.Models.Banners
{
    public partial class BannerListModel : BaseModel
    {
        [UrsDisplayName("Admin.Store.Banners.List.SearchBrandName")]
        
        public string SearchBannersName { get; set; }

        public ResponseResult Banners { get; set; }
    }
}