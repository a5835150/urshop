﻿using Urs.Data.Domain.Stores;
using Urs.Framework;
using Urs.Framework.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Urs.Framework.Models;
using Urs.Admin.Models.Users;

namespace Urs.Admin.Models.Settings
{
    public partial class StoreSettingsModel : BaseModel, ISettingsModel
    {
        public SelectList FeaturedGoodsOrderByValues { get; set; }

        public bool ShowGoodsSku { get; set; }

        public bool GoodsSearchFullDescEnabled { get; set; }

        public int GoodsSearchTermMinimumLength { get; set; }

        public int SearchPageGoodssPerPage { get; set; }

        public bool IncludeFeaturedGoodssInNormalLists { get; set; }

    }
}