﻿using Urs.Framework.Models;

namespace Urs.Admin.Models.Stores
{
    public partial class ShopModel : BaseEntityModel
    {
        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 纬度
        /// </summary>
        public decimal Latitude { get; set; }
        /// <summary>
        /// 经度
        /// </summary>
        public decimal Longitude { get; set; }
        /// <summary>
        /// 营业时间
        /// </summary>
        public string OpeningHours { get; set; }
        /// <summary>
        /// 发布/下架
        /// </summary>
        public bool Published { get; set; }
    }
}