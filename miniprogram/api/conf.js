const login = '/v1/user/login'
const banner = '/v1/common/banner'
const homePro = '/v1/goods/homegoods'
const productId = '/v1/goods/getbyid'
const getgoodsattr = '/v1/goods/getgoodsattr'
const getgoodsspec = '/v1/goods/getgoodsspec'
const category = '/v1/category/sublist'
const searCate = '/v1/search/findbycid'
const towishlist = '/v1/shoppingcart/towishlist'
const wishlist = '/v1/shoppingcart/wishlist'
const getShoppingcart = '/v1/shoppingcart/get'
const removecartitem = '/v1/shoppingcart/removecartitem'
const updatecartitem = '/v1/shoppingcart/updatecartitem'
const goodstocartbluk = '/v1/shoppingcart/goodstocartbluk'
const cartCount = '/v1/shoppingcart/count'
const cartallselected = '/v1/shoppingcart/cartallselected'
const goodstocart = '/v1/shoppingcart/goodstocart'
const goodstocheckout = '/v1/shoppingcart/goodstocheckout'
const paymentweixinwap = '/v1/plugin/weixinopenpaymenturl'
const orderList = '/v1/order/list'
const ordercancel = '/v1/order/cancel'
const search = '/v1/search/find'
const getcheckout = '/v1/checkout/get'
const submitcheckout = '/v1/checkout/submit'
const topic = '/v1/topic/detail'
const weixinopenlogin = '/v1/weixinopen/login'
const addressList = '/v1/address/list'
const formsubmit = '/v1/common/formsubmit'
const removewishlistitem = '/v1/shoppingcart/removewishlistitem'
const returnpost = '/v1/order/returnpost'
const orderDetail = '/v1/order/detail'
const checkminsubtotal = '/v1/checkout/checkminsubtotal'
const uploadpicture = '/v1/common/uploadpicture'
const topicList = '/v1/topic/list'
const customer = '/v1/user/submit'
const iscollect = '/v1/shoppingcart/iscollect'
const notice = '/v1/topic/detail'
const addressdefault = '/v1/address/default'
const addressadd = '/v1/address/add'
const addressedit = '/v1/address/edit'
const myinvitees = '/v1/user/myinvitees'
const myinvitecode = '/v1/user/myinvitecode'
const sendsmsforregister = '/v1/account/sendsmsforregister'
const accountlogin = '/v1/account/login'
const register = '/v1/account/register'
const pwdset = '/v1/account/pwdset'
const sendsmsforfindpwd = '/v1/account/sendsmsforregister'
const returnreasons = '/v1/common/returnreasons'
const returnactions = '/v1/common/returnactions'
const weixinopenphonenumber = '/v1/weixinopen/phonenumber'
const getcustomer = '/v1/user/get'
const exchange = '/v1/miniaccount/exchange'
const homecategories = '/v1/category/homecategories'
const gethotcate = '/v1/category/get'
const aftersales = '/v1/order/aftersales'
const getUnlimited = '/v1/weixinopen/getUnlimited'
const uploadavatar = '/v1/user/uploadavatar'
const gettemplates = '/v1/common/templates'
const minitemgoodsqty ='/v1/common/minitemgoodsqty'
const deliver ='/v1/order/deliver'
const shoplist = '/v1/shop/shoplist'
const checkshop ='/v1/user/checkshop'

const agentsupgrade = '/v1/agents/upgrade'
const agentspostinfo ='/v1/agents/postinfo'

module.exports = {
  login,
  banner,
  homePro,
  category,
  searCate,
  productId,
  towishlist,
  wishlist,
  getShoppingcart,
  removecartitem,
  updatecartitem,
  goodstocartbluk,
  cartCount,
  cartallselected,
  goodstocart,
  paymentweixinwap,
  orderList,
  ordercancel,
  search,
  getcheckout,
  submitcheckout,
  goodstocheckout,
  topic,
  weixinopenlogin,
  addressList,
  formsubmit,
  removewishlistitem,
  returnpost,
  orderDetail,
  checkminsubtotal,
  uploadpicture,
  topicList,
  customer,
  iscollect,
  notice,
  addressdefault,
  addressadd,
  addressedit,
  myinvitees,
  myinvitecode,
  sendsmsforregister,
  accountlogin,
  register,
  pwdset,
  sendsmsforfindpwd,
  returnreasons,
  returnactions,
  weixinopenphonenumber,
  getcustomer,
  exchange,
  homecategories,
  gethotcate,
  getgoodsattr,
  getgoodsspec,
  aftersales,
  getUnlimited,
  uploadavatar,
  gettemplates,
  minitemgoodsqty,
  deliver,
  shoplist,
  checkshop,
  agentsupgrade,
  agentspostinfo
}