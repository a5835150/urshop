import api from '../../api/api'
import { wishlist } from '../../api/conf'
import { removewishlistitem } from '../../api/conf'
Page({
  data:{
    list: []
  },
  onLoad:function(options){
    // 页面初始化 options为页面跳转所带来的参数
    wx.showLoading({
      title: '加载中',
    })
  },
  onReady:function(){
    // 页面渲染完成
    var that = this
    api.get(wishlist,{
      
    }).then(res=>{
      that.setData({
        list:res.Data.Items
      })
      wx.hideLoading()
    })
  },
  onShow:function(){
    // 页面显示
  },
  onHide:function(){
    // 页面隐藏
  },
  onUnload:function(){
    // 页面关闭
  },
  longTap: function (e) {
    var that = this
    wx.showModal({
      title: '提示',
      content: '是否取消收藏改商品',
      showCancel: true,
      success(res){
        if (res.confirm) {
          api.post(removewishlistitem + '?id=' + e.currentTarget.dataset.id,{
          }).then(res=>{
            that.onReady()
          })
        } else if (res.cancel) {
          
        }
      }
    })
  },
  tap:function(e){
    wx.navigateTo({
      url: '/pages/detail/detail?id=' + e.currentTarget.dataset.proid,
    })
  }
})