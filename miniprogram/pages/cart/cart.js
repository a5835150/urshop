import api from '../../api/api'
import {
  getShoppingcart
} from '../../api/conf'
import {
  removecartitem
} from '../../api/conf'
import {
  updatecartitem
} from '../../api/conf'
import {
  cartallselected
} from '../../api/conf'
import {
  cartCount
} from '../../api/conf'
import {
  checkminsubtotal
} from '../../api/conf'
import {
  searCate
} from '../../api/conf'
var app = getApp()
Page({
  data: {
    carList: [],
    OrderTotal: '',
    selected: false,
    idx: '',
    totalprice: 0,
    productlist: []
  },
  currentindex(e) {
    var r = e.target.dataset.index;
    this.setData({
      idx: r
    })
  },
  onLoad: function (options) {

    // 页面初始化 options为页面跳转所带来的参数
  },
  onShow: function () {
    this.getShoppingChart()
    this.getCartCount()
    this.getProductTj()
  },
  getShoppingChart: function () {
    var that = this
    api.get(getShoppingcart).then(res => {
      that.setData({
        carList: res.Data.Items,
        OrderTotal: res.Data.OrderTotal
      })
      that.isSelectAll()
      that.getTotalPrice()
    }).catch(err => {
    })
  },
  isSelectAll: function () {
    let carList = this.data.carList
    let SelectedTrue = []
    for (let i = 0; i < carList.length; i++) {
      if (carList[i].Selected) {
        SelectedTrue.push(carList[i].Selected)
      }
    }
    if (SelectedTrue.length == carList.length) {
      this.setData({
        selected: true
      })
    } else {
      this.setData({
        selected: false
      })
    }
  },
  getCartCount: function () {
    api.get(cartCount, {}).then(res => {
      let num = ''
      if (res.Data == 0) {
        num = ''
      } else {
        num = res.Data
      }
      // wx.setTabBarBadge({
      //   index: 2,
      //   text: num
      // })
    })
  },
  getCartNum: function () {
    let carList = this.data.carList
    let num = 0
    for (let j = 0; j < carList.length; j++) {
      if (carList[j].Selected) {

        num += carList[j].Qty
      }
    }
    // wx.setTabBarBadge({
    //   index: 2,
    //   text: num.toString()
    // })
  },
  getTotalPrice: function () {
    let that = this
    let carList = that.data.carList
    let total = 0
    for (let i = 0; i < carList.length; i++) {
      if (carList[i].Selected) {
        let str = carList[i].UnitPrice
        str = str.replace(/,/gi, "");
        let unitPrice = parseFloat((str).substr(1))
        total += carList[i].Qty * unitPrice
      }
    }
    that.setData({
      carList: carList,
      totalprice: total.toFixed(2)
    })
  },
  modalcnt: function (event) {
    var that = this
    wx.showModal({
      title: '提示',
      content: '确定移出购物车?',
      success: function (res) {
        if (res.confirm) {
          api.post(removecartitem + '?id=' + event.currentTarget.dataset.id, {}).then(res => {
            let arr = that.data.carList;
            let index = event.target.dataset.index
            let carList = that.data.carList.splice(index, 1)
            let total = 0
            for (let i = 0; i < arr.length; i++) {
              if (arr[i].Selected) {
                let str = arr[i].UnitPrice
                str = str.replace(/,/gi, "");
                let unitPrice = parseFloat((str).substr(1))
                total += arr[i].Qty * unitPrice
              }
            }
            that.setData({
              carList: arr,
              totalprice: total.toFixed(2)
            })       
          })
        } else if (res.cancel) {

        }
      }
    })
  },
  proInt:function(e){
    var that = this;
    var proNum = e.detail.value
    if (proNum < 10) {
      wx.showToast({
        title: '数量不能小于10',
        icon: 'none'
      })
      return
    }
    if (proNum > 99) {
      wx.showToast({
        title: '数量不能大于99',
        icon: 'none'
      })
      return
    }
    let selec = e.currentTarget.dataset.select
    that.setData({
      proNum: proNum
    })
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    api.post(updatecartitem, {
      Id: e.currentTarget.dataset.id,
      Quantity: that.data.proNum,
      Selected: selec
    }).then(res => {
      let idxs = e.currentTarget.dataset.index
      let carList = that.data.carList
      let Qty = carList[idxs].Qty
      if (Qty < 10) {
        wx.showToast({
          title: '数量不能小于10',
          icon: 'none'
        })
        return
      }
      var proNum = parseInt(e.detail.value) 
      if (e.detail.value < 10) {
        wx.showToast({
          title: '数量不能小于10',
          icon: 'none'
        })
        let updata = this.data.proNum
        this.setData({
          [updata]: 10
        })
      }
      carList[idxs].Qty = proNum
      that.setData({
        carList: carList
      })
      that.getCartNum()
      that.getTotalPrice()
      wx.hideLoading()
    })

  },
  addCart: function (event) {
    let that = this
    let qty = event.currentTarget.dataset.qty
    qty++;
    let selec = event.currentTarget.dataset.select
   
    if (qty > 99) {
      wx.showToast({
        title: '数量不能大于99',
        icon: 'none'
      })
      return
    }
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    api.post(updatecartitem, {
      Id: event.currentTarget.dataset.id,
      Quantity: qty,
      Selected: selec
    }).then(res => {
      let idxs = event.currentTarget.dataset.index
      let carList = that.data.carList
      let Qty = carList[idxs].Qty
      Qty = Qty+1
      carList[idxs].Qty = Qty
      that.setData({
        carList: carList
      })
      that.getTotalPrice()
      that.getCartNum()
      wx.hideLoading()
    }).catch(err => {
      wx.showToast({
        title: '商品已删除',
        icon: 'none'
      })
      wx.hideLoading()
    })
  },
  reduce: function (event) {
    let that = this
    let qty = event.currentTarget.dataset.qty
    let selec = event.currentTarget.dataset.select
    qty--;
    if (qty < 10) {
      wx.showToast({
        title: '数量不能小于10',
        icon: 'none'
      })
      return
    }
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    api.post(updatecartitem, {
      Id: event.currentTarget.dataset.id,
      Quantity: qty,
      Selected: selec
    }).then(res => {
      let idxs = event.currentTarget.dataset.index
      let carList = that.data.carList
      let Qty = carList[idxs].Qty
      if (Qty <= 10) {
        return
      }
      Qty = Qty - 1
      carList[idxs].Qty = Qty
      that.setData({
        carList: carList
      })
      that.getCartNum()
      that.getTotalPrice()
      wx.hideLoading()
    }).catch(err => {
      wx.showToast({
        title: '商品已删除',
        icon: 'none'
      })
      wx.hideLoading()
    })
  },
  Selected: function (event) {
    var that = this
    let qty = event.currentTarget.dataset.qty
    let selec = event.currentTarget.dataset.select
    selec = !selec
    api.post(updatecartitem, {
      Id: event.currentTarget.dataset.id,
      Quantity: qty,
      Selected: selec
    }).then(res => {
      const index = event.currentTarget.dataset.index
      let carList = that.data.carList
      const Selected = carList[index].Selected
      carList[index].Selected = !Selected
      that.setData({
        carList: carList
      })
      that.isSelectAll()
      that.getCartNum()
      that.getTotalPrice()
    }).catch(err => {
      wx.showToast({
        title: '商品已删除',
        icon: 'none'
      })
    })

  },
  allSelected: function (event) {
    var that = this
    let seleSwich = !that.data.selected
    api.post(cartallselected + '?selected=' + seleSwich, {}).then(res => {
      const index = event.currentTarget.dataset.index
      let carList = that.data.carList
      carList = carList.map(function (item, index, arr) {
        item.Selected = seleSwich;
        return item;
      })
      that.setData({
        carList: carList,
        selected: seleSwich
      })
      that.getCartNum()
      that.getTotalPrice()
    })
  },
  //结算检查
  checkminsubtotal: function () {
    var that = this
    api.get(checkminsubtotal, {}).then(res => {
      wx.navigateTo({
        url: '/pages/checkout/checkout',
      })
    }).catch(err => {
      wx.showToast({
        title: '至少勾选一件商品',
        icon: 'none'
      })
    })
  },
  goHome: function () {
    wx.switchTab({
      url: '/pages/index/index',
    })
  },
  getProductTj: function(){
    var that = this
    api.get(searCate,{
      cid: 243
    }).then(res=>{
      that.setData({
        productlist: res.Data
      })
    })
  }
})