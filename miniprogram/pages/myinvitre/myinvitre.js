import api from '../../api/api'
import { myinvitees } from '../../api/conf'

Page({

  data: {
    visitList: []
  },
  onLoad: function (options) {
    
  },
  onReady: function () {

  },
  onShow: function () {
    this.getmyinvitees()
  },
  getmyinvitees: function () {
    var that = this
    api.get(myinvitees,{

    }).then((res)=>{
      that.setData({
        visitList: res.Data
      })
    })
  }
})